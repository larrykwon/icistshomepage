import Head from 'next/head'
import Link from 'next/link';
import styles from './navigator.module.css';

export default function Navigator() {
  const table_of_contents_name = ["Home", "Experience", "About Us", "GRAFFITI", "Gallery", "Contents", "Contact"];
  const table_of_contents_url = ["/", "/experience", "/about-us", "/graffiti", "/gallery", "/contents", "/contact"];
  var table_of_contents = []
  for(var i = 0; i <table_of_contents_name.length; i++){
    table_of_contents.push([table_of_contents_name[i], table_of_contents_url[i]]);
  }
  return (
    <div className={styles.navigator}>
      {table_of_contents.map((content) => {
          return(
            <div className={styles.navItem} >
              <Link href={content[1]} key={content[0]}>
                <a>{content[0]}</a>
              </Link>
            </div>
          );
      })}
    </div>
  )
}