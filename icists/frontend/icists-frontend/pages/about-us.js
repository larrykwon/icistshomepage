import Head from 'next/head'
import Link from 'next/link';
import Navigator from '../components/navigator';

export default function AboutUs() {

  return (
    <div className="container">
      <Navigator/>
      About Us
    </div>
  )
}