import Head from 'next/head'
import Link from 'next/link';
import Navigator from '../components/navigator';

export default function Contents() {

  return (
    <div className="container">
      <Navigator/>
      Contents
    </div>
  )
}