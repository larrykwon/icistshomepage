package org.icists.icists;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcistsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IcistsApplication.class, args);
	}

}
