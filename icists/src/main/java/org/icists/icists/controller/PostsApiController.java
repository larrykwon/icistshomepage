package org.icists.icists.controller;

import lombok.RequiredArgsConstructor;
import org.icists.icists.dto.PostResponseDto;
import org.icists.icists.dto.PostSaveRequestDto;
import org.icists.icists.dto.PostUpdateRequestDto;
import org.icists.icists.service.posts.PostService;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
public class PostsApiController {
    private final PostService postService;

    @PutMapping("/api/posts/{id}")
    public Long update(@PathVariable Long id, @RequestBody PostUpdateRequestDto requestDto){
        return postService.update(id,requestDto);
    }

    @PostMapping("/api/posts/create")
    public Long save(@RequestBody PostSaveRequestDto requestDto){
        return postService.save(requestDto);
    }

    @GetMapping("/api/posts{id}")
    public PostResponseDto findById(@PathVariable Long id){
        return postService.findById(id);
    }
}
