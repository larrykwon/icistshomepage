package org.icists.icists.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class TestController {
    @GetMapping("/home")
    public String hello() {
        return "안녕하세요. 현재 서버시간은" + "입니다. \n";
    }
}
