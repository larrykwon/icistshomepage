package org.icists.icists.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.icists.icists.domain.posts.Post;

@Getter
@NoArgsConstructor
public class PostSaveRequestDto {

    private String category;
    private String title;
    private String content;
    private String author;

    @Builder
    public PostSaveRequestDto(String category, String title, String content, String author){
        this.category = category;
        this.title = title;
        this.content = content;
        this.author = author;
    }

    public Post toEntity() {
        return Post.builder()
                .category(category)
                .title(title)
                .content(content)
                .author(author)
        .build();
    }
}
