package org.icists.icists.service.posts;

import lombok.RequiredArgsConstructor;
import org.icists.icists.domain.posts.Post;
import org.icists.icists.domain.posts.PostRepository;
import org.icists.icists.dto.PostResponseDto;
import org.icists.icists.dto.PostSaveRequestDto;
import org.icists.icists.dto.PostUpdateRequestDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PostService {

    private final PostRepository postRepository;

    @Transactional
    public Long save(PostSaveRequestDto requestDto) {
        return postRepository.save(requestDto.toEntity()).getId();
    }

    @Transactional
    public Long update(Long id, PostUpdateRequestDto requestDto) {
        Optional<Post> postsOptional = postRepository.findById(id);
        postsOptional.ifPresent(posts -> {
            throw new IllegalArgumentException("해당 게시글이 없습니다. id =" + id);
        });
        Post post = postsOptional.get();
        post.update(requestDto.getCategory(),requestDto.getTitle(),requestDto.getContent());

        return id;
    }

    public PostResponseDto findById(Long id){
        Optional<Post> entityOptional = postRepository.findById(id);
        entityOptional.ifPresent(entity -> {
            throw new IllegalArgumentException("해당 게시글이 없습니다. id =" + id);
        });
        Post entity = entityOptional.get();
        return new PostResponseDto(entity);
    }

}
